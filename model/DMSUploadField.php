<?php
namespace HotHouse\DMS4;

use SilverStripe\AssetAdmin\Controller\AssetAdmin;
use SilverStripe\Assets\File;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Bummzack\SortableFile\Forms\SortableUploadField;
use SilverStripe\ORM\DataObjectInterface;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\ManyManyList;
use SilverStripe\ORM\ManyManyThroughList;
use SilverStripe\ORM\UnsavedRelationList;

class DMSUploadField extends SortableUploadField {

    private static $allowed_actions = [
        'upload'
    ];

    protected $DMSExtensionObject;

    public function upload(HTTPRequest $request)
    {
        if ($this->isDisabled() || $this->isReadonly()) {
            return $this->httpError(403);
        }

        // CSRF check
        $token = $this->getForm()->getSecurityToken();
        if (!$token->checkRequest($request)) {
            return $this->httpError(400);
        }

        $tmpFile = $request->postVar('Upload');
        /** @var File $file */
        $file = $this->saveTemporaryFile($tmpFile, $error);

        // Prepare result
        if ($error) {
            $result = [
                'message' => [
                    'type' => 'error',
                    'value' => $error,
                ]
            ];
            $this->getUpload()->clearErrors();
            return (new HTTPResponse(json_encode($result), 400))
                ->addHeader('Content-Type', 'application/json');
        }

        // We need an ID for getObjectFromData
        if (!$file->isInDB()) {
            $file->write();
        }

        $this->attachToDMS($file);

        // Return success response
        $result = [
            AssetAdmin::singleton()->getObjectFromData($file)
        ];

        // Don't discard pre-generated client side canvas thumbnail
        if ($result[0]['category'] === 'image') {
            unset($result[0]['thumbnail']);
        }
        $this->getUpload()->clearErrors();
        return (new HTTPResponse(json_encode($result)))
            ->addHeader('Content-Type', 'application/json');
    }

    public function setDMSExtensionObject($object) {
        $this->DMSExtensionObject = $object;
    }

    protected function attachToDMS (File $file) {
        $page = $this->getRecord();
        $targetFolder = $page->getOrCreateMainFolder();
        $file->ParentID = $targetFolder->ID;
        $file->updateFilesystem();
        $file->Write();
    }

    public function saveInto(DataObjectInterface $record)
    {
        parent::saveInto($record);

        // Check required relation details are available
        $fieldname = $this->getName();
        $idList = $this->getItemIDs();
        $rawList = $this->rawSubmittal;

        $page = $this->getRecord();
        // safety, only allow deleting files in pages auto-generated folder
        $hasOwnMainFolder = $this->DMSExtensionObject->getMainFolder($page, true);
        $existingFiles = $this->DMSExtensionObject->getMainFiles($page);
        $existingFileIDs = array_map(function($item) {
            return $item->ID;
        }, $existingFiles->toArray());

        if (!$fieldname || (empty($existingFileIDs) && !is_array($rawList))) {
            return $this;
        }


        $sortColumn = $this->getSortColumn();

        // Check type of relation
        $relation = $record->hasMethod($fieldname) ? $record->$fieldname() : null;
        if ($relation) {
            $idList = $this->getItemIDs();
            $rawList = $this->rawSubmittal;
            $sortColumn = $this->getSortColumn();

            if ($relation instanceof ManyManyList) {
                try {
                    // Apply the sorting, wrapped in a transaction.
                    // If something goes wrong, the DB will not contain invalid data
                    DB::get_conn()->withTransaction(function () use ($relation, $idList, $rawList, $record, $sortColumn) {
                        $this->sortManyManyRelation($relation, $idList, $rawList, $record, $sortColumn);
                    });
                } catch (\Exception $ex) {
                    $this->logger->warning('Unable to sort files in sortable relation.', ['exception' => $ex]);
                }
            } elseif ($relation instanceof ManyManyThroughList) {
                try {
                    // Apply the sorting, wrapped in a transaction.
                    // If something goes wrong, the DB will not contain invalid data
                    DB::get_conn()->withTransaction(function () use ($relation, $idList, $rawList, $sortColumn) {
                        $this->sortManyManyThroughRelation($relation, $idList, $rawList, $sortColumn);
                    });
                } catch (\Exception $ex) {
                    $this->logger->warning('Unable to sort files in sortable relation.', ['exception' => $ex]);
                }
            } elseif ($relation instanceof UnsavedRelationList) {
                // With an unsaved relation list the items can just be removed and re-added
                $sort = 0;
                $relation->removeAll();
                foreach ($rawList as $id) {
                    if (in_array($id, $idList)) {
                        $relation->add($id, [$sortColumn => $sort++]);
                    }
                }
            }
        } else {
            if (File::create()->hasField($sortColumn)) {
                $sort = 0;
                $filesToDelete = [];
                foreach ($existingFileIDs as $existingID) {
                    if (array_search($existingID, $idList) === false) {
                        $filesToDelete[] = $existingID;
                    }
                }
                foreach ($idList as $fileID) {
                    $file = File::get()->filter('ID', $fileID)->first();
                    if ($file) {
                        $file->$sortColumn = $sort;
                        $file->write();
                        $sort++;
                    }
                }
                if ($hasOwnMainFolder) {
                    foreach ($filesToDelete as $deleteID) {
                        $fileToDelete = File::get()->filter('ID', $deleteID)->first();
                        if ($fileToDelete) {
                            $fileToDelete->doUnpublish();
                            $fileToDelete->delete();
                        }
                    }
                }
            }
        }

        return $this;
    }
}
