# silverstripe-4-dms

## Usage

This module gives the ability to link folders (and containing files) to any page.
You can link multiple folders to a page. You can drag/drop to set the order of display for the folders. 
The folder title is used to display as a section heading.   

## Setup

- install via composer
- Add $DMS4 to any template to display files attached to the page

##Extending

to adjust the template, copy the DMS4.ss to your templates folder (root folder).
