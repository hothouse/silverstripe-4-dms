<?php
namespace HotHouse\DMS4;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Folder;
use SilverStripe\Assets\FolderNameFilter;
use SilverStripe\Control\Controller;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldDataColumns;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\NumericField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\PaginatedList;
use SilverStripe\View\Parsers\URLSegmentFilter;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Symbiote\GridFieldExtensions\GridFieldTitleHeader;

class DMS4Extension extends DataExtension {

    private static $db = [
        'DMSFilesSortBy' => 'Varchar(255)',
        'PaginateResults' => 'Boolean',
        'DMSItemsPerPage' => 'Int',
        'DMSCombineFolders' => 'Boolean'
    ];

    private static $many_many = [
        'DMSFolders' => Folder::class
    ];

    private static $many_many_extraFields = [
        'DMSFolders' => [
            'SortOrder' => 'Int',
            'Main' => 'Boolean'
        ]
    ];

    private static $defaults = [
        'DMSItemsPerPage' => 5
    ];

    private static $owns = [
        'DMSFolders'
    ];

    public static function get_gridfieldconfig($SortFieldName = 'SortOrder') {
        $gridFieldConfig = GridFieldConfig::create()->addComponents(
            new GridFieldToolbarHeader(),
            new GridFieldAddExistingAutocompleter(),
            $SortFieldName && class_exists(GridFieldTitleHeader::class) ? new GridFieldTitleHeader(
            ) : new GridFieldSortableHeader(),
            new GridFieldDataColumns(),
            new GridFieldPaginator(20),
            new GridFieldDeleteAction(true)
        );
        if ($SortFieldName) {
            if (class_exists(GridFieldOrderableRows::class)) {
                $gridFieldConfig->addComponent(new GridFieldOrderableRows($SortFieldName));
            } elseif (class_exists('GridFieldSortableRows')) {
                $gridFieldConfig->addComponent(new GridFieldSortableRows($SortFieldName));
            }
        }
        return $gridFieldConfig;
    }

    public function updateCMSFields(FieldList $fields)
    {
		$config = self::get_gridfieldconfig();
		$config->getComponentByType(GridFieldAddExistingAutocompleter::class)
			->setResultsLimit(100)
			->setResultsFormat('$getBreadcrumbs(\' > \')');
		$config->getComponentByType(GridFieldDataColumns::class)
			->setDisplayFields([
				'BreadCrumbsTitle' => 'Folder',
				'FileNames' => 'Files'
			]);
            
		$folders = $this->owner->DMSFolders();
		$folderCount = $folders->count();
		if ($folderCount > 0) {
    		$fields->addFieldToTab('Root.Documents', GridField::create('Folders', 'Document Folders', $folders, $config));
        }
        
        $uploadField = DMSUploadField::create('UploadFile', 'Upload File', $this->getMainFiles())->setSortColumn('SortID');
        $uploadField->setDMSExtensionObject($this);
        $fields->addFieldToTab('Root.Documents', $uploadField);

        $sortArray = [
            'Title ASC' => 'Title Ascending',
            'Title DESC' => 'Title Descending',
            'Created ASC' => 'Created Ascending',
            'Created DESC' => 'Created Descending',
            'LastEdited ASC' => 'LastEdited Ascending',
            'LastEdited DESC' => 'LastEdited Descending',
        ];
        $fields->addFieldToTab('Root.Documents', DropdownField::create('DMSFilesSortBy', 'Sort by', $sortArray));

        $fields->addFieldToTab('Root.Documents', CheckboxField::create('DMSCombineFolders', 'Combine Files in single list'));
        $fields->addFieldToTab('Root.Documents', CheckboxField::create('PaginateResults', 'Use Pagination'));

        $itemsPerPageField = NumericField::create('DMSItemsPerPage', 'Items per page');
        $fields->addFieldToTab('Root.Documents', $itemsPerPageField);

//        if ($this->getMainFiles()->count()) {
//            $fields->addFieldToTab('Root.Documents', GridField::create('MainFileList', 'Page Files (only files from main folder)', $this->getMainFiles(), self::get_gridfieldconfig(null)));
//        }
        parent::updateCMSFields($fields);
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if (!$this->owner->DMSItemsPerPage) {
            $this->owner->DMSItemsPerPage = self::$defaults['DMSItemsPerPage'];
        }
    }

    public function HasFiles() {
        if (!$this->owner->DMSFolders()->Count()) return false;
        foreach ($this->owner->DMSFolders() as $folder) {
            if ($folder->myChildren()->exclude('ClassName', 'SilverStripe\Assets\Folder')->exists()) return true;
        }
        return false;
    }

	public function getDMS4FileList() {
//		$documents = ArrayList::create();
//		foreach ($this->owner->DMSFolders()->sort('SortOrder') as $folder) {
//			foreach ($folder->myChildren()->sort($this->owner->DMSFilesSortBy) as $file) {
//				if (is_a($file, Folder::class)) {
//					continue;
//				}
//				$documents->push($file);
//			}
//		}
        $documentIDS = [];
        foreach ($this->owner->DMSFolders()->sort('SortOrder') as $folder) {
            foreach ($folder->myChildren()->sort($this->owner->DMSFilesSortBy) as $file) {
                if (is_a($file, Folder::class)) {
                    continue;
                }
                $documentIDS[] = $file->ID;
            }
        }
        $documents = empty($documentIDS) ? ArrayList::create() : File::get()->filter('ID', $documentIDS);
		return $documents;
	}

	public function getPaginatedDMS4FileList($sort = null) {
		$documents = $this->getDMS4FileList()->sort($sort);
        return PaginatedList::create($documents, Controller::curr()->getRequest())->setPageLength($this->owner->DMSItemsPerPage);
	}

    public function getMainFiles($owner = null) {
        $mainFolder = $this->getMainFolder($owner);
        return $mainFolder ? $mainFolder->Children()->exclude('ClassName', 'SilverStripe\Assets\Folder') : ArrayList::create();
    }

    public function DMS4() {
        return $this->owner->renderWith('DMS4');
    }

    public function DMSItemsPerPageWithFallback() {
        return ($this->owner->DMSItemsPerPage > 0) ? $this->owner->DMSItemsPerPage : self::$defaults['DMSItemsPerPage'];
    }

    public function getMainFolder($owner, $onlyMainTagged = false) {
        if ($owner) {
            $this->owner = $owner;
        }
        $mainFolder = null;
        if ($this->owner->DMSFolders()->count() == 1) {
            if ($onlyMainTagged) {
                $folder = $this->owner->DMSFolders()->filter('Main', true)->first();
            } else {
                $folder = $this->owner->DMSFolders()->first();
            }
            $mainFolder = $folder;
        }  else {
            $mainFolderList = $this->owner->DMSFolders()->filter('Main', true)->sort('SortOrder');
            if ($mainFolderList->count()) $mainFolder = $mainFolderList->first();
        }

        return $mainFolder;
    }

    public function getOrCreateMainFolder($owner = null) {
        if ($owner) {
            $this->owner = $owner;
        }
        $mainFolder = null;
        if ($this->owner->DMSFolders()->count() == 1) {
            $folder = $this->owner->DMSFolders()->first();
            $mainFolder = $folder;
        } elseif ($this->owner->DMSFolders()->count() == 0) {
            $folder = $this->createFolder();
            $this->owner->DMSFolders()->add($folder, [
                'Main' => true
            ]);
            $mainFolder = $folder;
        } else {
            $mainFolder = $this->owner->DMSFolders()->filter('Main', true);
            if ($mainFolder->count()) $mainFolder = $mainFolder->first();
            else {
                $mainFolder = $this->owner->DMSFolders()->first();
                $this->owner->DMSFolders()->add($mainFolder, [
                    'Main' => true
                ]);
            }
        }
        if ($mainFolder->Name !== FolderNameFilter::create()->filter($this->owner->Title)) {
            // only update folder name before files or other folders have been added
            if ($this->owner->DMSFolders()->count() == 1) {
                if ($mainFolder->Children()->exclude('ClassName', 'SilverStripe\Assets\Folder')->count() == 0) {
                    //check if other pages reference this folder - don't touch if they do
                    $SQL = 'SELECT DISTINCT SiteTreeID FROM SiteTree_DMSFolders WHERE FolderID = ' . $mainFolder->ID;
                    $result = DB::query($SQL)->column('SiteTreeID');
                    if (count($result) == 1) {
                        $mainFolder->Title = $this->owner->Title;
                        $mainFolder->write();
                    }
                }
            }
        }
        return $mainFolder;
    }

    protected function createFolder($folderToMove = null) {
        //create folder structure
        $mainFolder = 'DMS';
        $obj = $this->getOwner();
        if (!$folderToMove) {
            $parents = [$obj->Title];
        }
        while($obj->ParentID) {
            $parents[] = $obj->Parent()->Title;
            $obj = $obj->Parent();
        }
        $parents[] = $mainFolder;
        $parentFolderID = 0;
        foreach (array_reverse($parents) as $folderName) {
            $folder = Folder::get()->filter([
                'ParentID' => $parentFolderID,
                'Name' => FolderNameFilter::create()->filter($folderName)
            ])->first();
            if (!$folder) {
                $folder = new Folder();
                $folder->setTitle($folderName);
                $folder->ParentID = $parentFolderID;
                $folder->write();
            }
            $parentFolderID = $folder->ID;
        }
        if ($folderToMove) {
            $folderToMove->ParentID = $folder->ID;
            $folderToMove->write();
        }

        return $folder;
    }

    public function onAfterPublish($record) {
        foreach ($this->getMainFiles() as $file) {
            $file->publishRecursive();
        }
    }

    public function onAfterWrite()
    {
        $changed = $this->owner->getChangedFields();
        if (isset($changed['ParentID']) && $changed['ParentID']['before'] != $changed['ParentID']['after']) {
            //move folder
            $mainFolder = $this->owner->DMSFolders()->filter('Main', true);
            if ($mainFolder->count() == 1) {
                $mainFolder = $mainFolder->first();
                $this->createFolder($mainFolder);
            }
        }
    }
}
