<?php


namespace HotHouse\DMS4;


use League\Flysystem\Config;
use SilverStripe\Assets\File;
use SilverStripe\ORM\DataExtension;

class DMS4FileExtension extends DataExtension {

    private static $db = array(
        'SortID' => 'Int'
    );

    public function getDownloadTag()
    {
        return $this->owner->renderWith('DBFile_download');
    }

    public function getViewTag()
    {
        return $this->owner->renderWith('DBFile_view');
    }

    public function getExtendedFileType() {
        $return = File::get_file_type($this->owner->getFilename());
        if ($return == 'unknown') {
            $configValues = \SilverStripe\Core\Config\Config::forClass('SilverStripe\Assets\File');
            if ($configValues) $configValues = $configValues->get('file_types');
            $return = isset($configValues[strtolower(File::get_file_extension($this->owner->getFilename()))]) ?
                $configValues[strtolower(File::get_file_extension($this->owner->getFilename()))] :
                $return;
        }
        return $return;
    }
}
