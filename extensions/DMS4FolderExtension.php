<?php

namespace HotHouse\DMS4;

use SilverStripe\Assets\File;
use SilverStripe\Control\Controller;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class DMS4FolderExtension extends DataExtension {

    public function updateFormFields($fields, $controller, $formName, $context) {
        $fields->insertBefore('Editor.Details.Name', TextField::create("DMSTitle", File::singleton()->fieldLabel('Title')));
    }
}
