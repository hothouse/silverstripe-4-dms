<?php


namespace HotHouse\DMS4;


use SilverStripe\Control\Controller;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\PaginatedList;

class DMS4FolderFieldsExtension extends DataExtension {
	private static $db = [
		'DMSTitle' => 'Varchar(255)'
	];

	private static $summary_fields = [
		'DMSTitle' => 'Title',
		'Name' => 'Name'
	];

	private static $casting = [
		'FileNames' => 'HTMLFragment'
	];

	public function paginatedChildren($sort, $paginate = false, $itemsPerPage = 5) {
		$children = $this->owner->myChildren()->sort($sort);
		if ($paginate) {
			return PaginatedList::create($children, Controller::curr()->getRequest())->setPageLength($itemsPerPage);
		} else {
			return $children;
		}
	}

	public function BreadCrumbsTitle() {
		return $this->owner->getBreadCrumbs(' > ');
	}

	public function FileNames() {
		$children = $this->owner->myChildren();
		return DBField::create_field('HTMLText', implode('<br>', $children->column('Name')));
	}
}
